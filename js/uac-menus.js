/**
 * Created by Andy on 9/10/2016.
 */

import dashboard from "../../core/js/dashboard.js";

export default () => {
    dashboard.addMenu("uac", {
        title: "UAC",
        icon: "fa fa-lock",
        url: "",
        location: 'left',
    });
    dashboard.addMenu("uac:users", {
        title: "Users",
        icon: "fa fa-users",
        url: "#user",
    });
    dashboard.addMenu("uac:roles", {
        title: "Roles",
        icon: "fa fa-sitemap",
        url: "#role",
    });
    dashboard.addMenu("uac:permissions", {
        title: "Permissions",
        icon: "fa fa-key",
        url: "#permission",
    });
}
