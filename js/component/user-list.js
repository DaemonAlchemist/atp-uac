/**
 * Created by Andy on 9/8/2016.
 */

import router from "../../../core/js/router.js";
import load from "../../../core/js/loader.js";
import rest from "../../../rest/js/rest.js";
import displayUserDetails from "./user-details.js";

export default ([userTemplate, users, roles, container]) => {
    console.log("User list resources loaded");
    container.html(userTemplate({users}));

    //Create new user
    container.find("#new-user-form").restForm().then(() => router.run('user'));

    //Delete user
    container.find(".user-delete-btn").click(function() {
        let id = $(this).data('id');
        rest.delete("user/" + id).then(() => router.run('user'));
    });

    //Open details panel
    container.find("div.user-list-item").click(function() {
        let userId = $(this).data('id');
        let details = $("#user-details-" + userId);
        let spinner = $("#user-spinner-" + userId);
        if($(this).data('open')) {
            details.hide('fast');
            spinner.hide('fast');
        } else {
            spinner.show('fast');
            Promise.all([
                load.template('module/uac/views/user/details'),
                rest.get("user/" + userId + "/details"),
                roles,
                details
            ])
            .then(displayUserDetails)
            .then(() => {
                details.show('fast');
                spinner.hide('fast');
            })
        }
        $(this).data('open', !$(this).data('open'));
    });
};
