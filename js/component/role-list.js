/**
 * Created by Andy on 9/8/2016.
 */

import router from "../../../core/js/router.js";
import load from "../../../core/js/loader.js";
import rest from "../../../rest/js/rest.js";
import displayRoleDetails from "./role-details.js";

export default ([roleTemplate, roles, permissions, container]) => {
    console.log("Role list resources loaded");
    container.html(roleTemplate({roles}));

    //Create new role
    container.find("#new-role-form").restForm().then(() => router.run('role'));

    //Delete role
    container.find(".role-delete-btn").click(function() {
        let id = $(this).data('id');
        rest.delete("role/" + id).then(() => router.run('role'));
    });

    //Open details panel
    container.find("div.role-list-item").click(function() {
        let roleId = $(this).data('id');
        let details = $("#role-details-" + roleId);
        let spinner = $("#role-spinner-" + roleId);
        if($(this).data('open')) {
            details.hide('fast');
            spinner.hide('fast');
        } else {
            spinner.show('fast');
            Promise.all([
                load.template('module/uac/views/role/details'),
                rest.get("role/" + roleId + "/details"),
                permissions,
                details
            ])
            .then(displayRoleDetails)
            .then(() => {
                details.show('fast');
                spinner.hide('fast');
            })
        }
        $(this).data('open', !$(this).data('open'));
    });
};
