/**
 * Created by Andy on 9/8/2016.
 */

import router from "../../../core/js/router.js";
import load from "../../../core/js/loader.js";
import rest from "../../../rest/js/rest.js";

export default ([permissionTemplate, permissions, container]) => {
    console.log("Permission list resources loaded");
    container.html(permissionTemplate({permissions}));

    //Create new permission
    container.find("#new-permission-form").restForm().then(() => router.run('permission'));

    //Delete permission
    container.find(".permission-delete-btn").click(function() {
        let id = $(this).data('id');
        rest.delete("permission/" + id).then(() => router.run('permission'));
    });
};
