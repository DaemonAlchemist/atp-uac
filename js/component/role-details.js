/**
 * Created by Andy on 9/8/2016.
 */

import rest from "../../../rest/js/rest.js";

export default ([detailsTemplate, roleDetails, allPermissions, container]) => {
    //Combine user permissions and all permissions
    roleDetails.combinedPermissions = allPermissions
        .map(p => $.extend({}, p, {
            assigned: roleDetails.permissions.filter(rP => rP.name == p.name).length > 0
        }));

    //Display the details panel
    container.html(detailsTemplate(roleDetails));

    //Toggle permissions
    container.find(".permission-toggle").click(function() {
        let toggle = $(this);
        let data = toggle.data();
        if (data.assigned) {
            rest.delete("role/" + roleDetails.id + "/permission/" + data.permissionId)
                .then(() => {
                    console.log("Permission " + data.permissionId + " removed from role " + roleDetails.id);
                    toggle.data('assigned', false);
                    toggle.addClass('btn-danger').removeClass('btn-success');
                });
        } else {
            rest.post("role/" + roleDetails.id + "/permission", {data})
                .then(() => {
                    console.log("Permission " + data.permissionId + " added to role " + roleDetails.id);
                    toggle.data('assigned', true);
                    toggle.addClass('btn-success').removeClass('btn-danger');
                });
        }
    });
};
