/**
 * Created by Andy on 9/8/2016.
 */

import rest from "../../../rest/js/rest.js";

export default ([detailsTemplate, userDetails, allRoles, container]) => {
    //Combine user roles and all roles
    userDetails.combinedRoles = allRoles
        .map(r => $.extend({}, r, {
            assigned: userDetails.roles.filter(uR => uR.name == r.name).length > 0
        }));

    //Display the details panel
    container.html(detailsTemplate(userDetails));

    //Toggle permissions
    container.find(".role-toggle").click(function() {
        let toggle = $(this);
        let data = toggle.data();
        if (data.assigned) {
            rest.delete("user/" + userDetails.id + "/role/" + data.roleId)
                .then(() => {
                    console.log("Role " + data.roleId + " removed from user " + userDetails.id);
                    toggle.data('assigned', false);
                    toggle.addClass('btn-danger').removeClass('btn-success');
                });
        } else {
            rest.post("user/" + userDetails.id + "/role", {data})
                .then(() => {
                    console.log("Role " + data.roleId + " added to user " + userDetails.id);
                    toggle.data('assigned', true);
                    toggle.addClass('btn-success').removeClass('btn-danger');
                });
        }
    });
};
