/**
 * Created by Andy on 8/20/2016.
 */

import userController from "./controller/user-controller.js";
import roleController from "./controller/role-controller.js";
import permissionController from "./controller/permission-controller.js";
import uacMenuItems from "./uac-menus.js";

export default () => {
    userController();
    roleController();
    permissionController();
    uacMenuItems();
}