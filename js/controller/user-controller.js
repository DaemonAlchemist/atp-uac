/**
 * Created by Andy on 9/12/2016.
 */

import router from "../../../core/js/router.js";
import load from "../../../core/js/loader.js";
import rest from "../../../rest/js/rest.js";
import app from "../../../core/js/app.js";
import displayUserList from "../component/user-list.js";

export default () => {
    router.addRoute('^user$', () => {
        console.log("User list controller");
        Promise.all([
            load.template("module/uac/views/user/list"),
            rest.get("user", {data: {sort: "user_name ASC"}}),
            rest.get('role', {data: {sort: "name ASC"}}),
            app.pageContainer()
        ])
            .then(displayUserList);
    })
}