/**
 * Created by Andy on 8/28/2016.
 */

import router from "../../../core/js/router.js";
import load from "../../../core/js/loader.js";
import rest from "../../../rest/js/rest.js";
import app from "../../../core/js/app.js";
import displayPermissionList from "../component/permission-list.js";

export default () => {
    router.addRoute('^permission$', () => {
        console.log("Permission list controller");
        Promise.all([
            load.template("module/uac/views/permission/list"),
            rest.get("permission", {data: {sort: "name ASC"}}),
            app.pageContainer()
        ])
        .then(displayPermissionList);
    })
}