/**
 * Created by Andy on 8/28/2016.
 */

import router from "../../../core/js/router.js";
import load from "../../../core/js/loader.js";
import rest from "../../../rest/js/rest.js";
import app from "../../../core/js/app.js";
import displayRoleList from "../component/role-list.js";

export default () => {
    router.addRoute('^role$', () => {
        console.log("Role list controller");
        Promise.all([
            load.template("module/uac/views/role/list"),
            rest.get("role", {data: {sort: "name ASC"}}),
            rest.get('permission', {data: {sort: "name ASC"}}),
            app.pageContainer()
        ])
        .then(displayRoleList);
    })
}